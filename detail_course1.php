<?php
session_start();

// Dummy data kursus
$courses = array(
    "UI UX Design" => array(
        "Description" => "Course ini adalah kursus yang dirancang untuk mengajarkan konsep dasar dan praktik terbaik dalam desain antarmuka pengguna (UI) dan pengalaman pengguna (UX). Dalam kursus ini, peserta akan mempelajari prinsip-prinsip desain yang efektif untuk menciptakan pengalaman pengguna yang memikat dan fungsional.",
        "Materials" => array(
            "Pengantar UI UX Design",
            "Prinsip Desain",
            "Proses Desain"
        )
    ),
    "Data Science" => array(
        "Description" => "Description 2",
        "Materials" => array(
            "Material 2.1",
            "Material 2.2",
            "Material 2.3"
        )
    ),
    "Pemrograman Web" => array(
        "Description" => "Description 3",
        "Materials" => array(
            "Material 3.1",
            "Material 3.2",
            "Material 3.3"
        )
    )
);

// Check if user is not logged in, redirect to login page
if (!isset($_SESSION['user'])) {
    header('Location: login.html');
    exit();
}

// Mendapatkan judul kursus yang dipilih
if (isset($_GET['title']) && array_key_exists($_GET['title'], $courses)) {
    $selectedCourse = $_GET['title'];
} else {
    header('Location: home.php'); // Redirect jika judul kursus tidak valid
    exit();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title><?php echo $selectedCourse; ?> Detail</title>

</head>

<body>

    <body>
        <header>
            <nav class="navbar">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="courses.php">Courses</a></li>
                    <li><a href="Logout.php">Logout</a></li>
                </ul>
            </nav>

        </header>
        <main>
            <section class="course-detail">
                <div
                    style="width: 100%; height: 100%; flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 16px; display: inline-flex">
                    <div
                        style="color: #4EA17D; font-size: 20px; font-family: Poppins; font-weight: 700; word-wrap: break-word">
                        KATEGORI > UI UX Design</div>
                    <div
                        style="color: black; font-size: 32px; font-family: Poppins; font-weight: 700; word-wrap: break-word">
                        Ini dia yang kamu cari!!</div>
                    <div
                        style="width: 592px; color: #D9D9D9; font-size: 16px; font-family: Poppins; font-weight: 400; word-wrap: break-word">
                        Selamat belajar!!!</div>
                </div>
                <h2><?php echo $selectedCourse; ?></h2>
                <p><strong>Description:</strong> <?php echo $courses[$selectedCourse]['Description']; ?></p>
                <h3>Materials:</h3>
                <div class="catalog">
                    <?php foreach ($courses[$selectedCourse]['Materials'] as $material): ?>
                        <div class="card">
                            <h3>MATERI</h3>
                            <h2><?php echo $material; ?></h2>
                            <p>Description of <?php echo $material; ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
                <h3>Upload Task:</h3>
                <!-- Form untuk upload file tugas -->
                <form action="upload_task.php" method="post" enctype="multipart/form-data">
                    <label for="task_file">Choose File:</label>
                    <input type="file" id="task_file" name="task_file" required>
                    <input type="submit" value="Upload Task">
                </form>
            </section>
        </main>
        <footer>
            <p>&copy; 2024 EduTechHub. All rights reserved.</p>
        </footer>
    </body>

</html>