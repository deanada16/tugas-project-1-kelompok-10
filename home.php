<?php
session_start();

// Data kursus disimpan dalam array (contoh)
$courses = array(
    array("UI UX Design", "UI/UX adalah kursus online yang mengajarkan prinsip-prinsip dan praktik terbaik desain antarmuka pengguna (UI) dan pengalaman pengguna (UX). Kursus ini mencakup topik seperti penelitian pengguna, wireframing, prototyping, desain visual, dan pengujian kegunaan."),
    array("Data Science", "Data Science adalah bidang lintas disiplin yang menggabungkan statistik, pembelajaran mesin, dan visualisasi data untuk mengekstrak wawasan dan pengetahuan dari data. Data science melibatkan pembersihan, persiapan, dan analisis data untuk mengidentifikasi pola, tren, dan hubungan. Bidang ini juga melibatkan penyampaian temuan kepada pemangku kepentingan melalui visualisasi data dan cara lainnya. Ilmu ini digunakan dalam berbagai industri, termasuk kesehatan, keuangan, pemasaran, dan teknologi."),
    array("Pemrograman Website", "Pemrograman dirancang untuk mengajarkan prinsip-prinsip dasar dan lanjutan dalam menulis kode komputer. Materi kursus ini mencakup konsep dasar pemrograman, seperti variabel, kondisi, perulangan, dan fungsi, serta topik-topik lanjutan seperti pemrograman berorientasi objek, pengembangan web, dan pengembangan aplikasi mobile."),
    // Tambahkan data kursus lainnya jika diperlukan
);

// Check if user is not logged in, redirect to login page
if (!isset($_SESSION['user'])){
  header('Location: login.html');
  exit();
}
?>


<!DOCTYPE html>
<html lang="en"><style>
        /* CSS untuk tampilan yang menarik */
        body {
            font-family: Arial, sans-serif;
            background-color: #f5f5f5;
            margin: 0;
            padding: 20px;
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
        }
        header {
            background-color: #FFFFFF;
            color: white;
            padding: 10px;
            text-align: center;
            border-radius: 8px 8px 0 0;
        }
        nav {
            background-color: #333;
            overflow: hidden;
            border-radius: 8px;
        }
        nav a {
            float: left;
            display: block;
            color: white;
            text-align: center;
            padding: 14px 20px;
            text-decoration: none;
        }
        nav a:hover {
            background-color: #ddd;
            color: black;
        }
        main {
            padding: 20px;
        }
        section {
            margin-bottom: 20px;
        }
        h1, h2 {
            color: #333;
        }
        p {
            color: #666;
            line-height: 1.6;
        }
        footer {
            text-align: center;
            margin-top: 20px;
            padding: 10px;
            background-color: #FFFFFF;
            color: white;
            border-radius: 0 0 8px 8px;
        }
    </style>
<head>
    <link rel="stylesheet" href="style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EduTechHub - Home</title>
    <link rel="stylesheet" href="style.css">
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }
        
        th, td {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        th {
            background-color: #76b583;
        }
        /* Style untuk tautan pada baris tabel */
        tr:hover {
            background-color: #5f9e8f;
            cursor: pointer;
        }
        body {
            font-family: Arial, sans-serif;
            margin: 15px;
        }
        .container {
          
            max-width: 800px;
            margin: 0 auto;
        }
        h1 {
            color: #333;
        }
        p {
            line-height: 1.6;
        }
    </style>
    </style>
</head>
<body>
    <header>
        <nav class="navbar">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="courses.php">Courses</a></li>
                <li><a href="Logout.php">Logout</a></li>
            </ul>
        </nav>
    </header>
    <main>
    <div class="container">
        <header>
            <h1>Welcome to Our Edutech</h1>
            <p>Selamat Belajar!!</p>
        </header>
        <section class="course-list">
            <h1> Available Courses</h1>
            <br> <br>
            <!-- Tabel untuk menampilkan daftar kursus -->
            <table>
                <thead>
                    <tr>
                        <th>Course Title</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // Loop untuk menampilkan data kursus dalam tabel
                    foreach ($courses as $course) {
                        // Mendapatkan judul kursus untuk digunakan dalam tautan
                        $courseTitle = $course[0];
                        // Menambahkan tautan pada setiap baris tabel yang mengarah ke halaman detail kursus
                        echo "<tr onclick=\"window.location='detail_course1.php?title=$courseTitle';\">";
                        foreach ($course as $detail) {
                            echo "<td>$detail</td>";
                        }
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
        </section>
        <section>
                <h2>About Us</h2>
                <p>Edutech merupakan... </p>
            
        <section>
            <h2>Our Services</h2>
            <ul>
                <li>Service 1</li>
                <li>Service 2</li>
                <li>Service 3</li>
            </ul>
        </section>
    </main>
    <footer>
        <p>&copy; 2024 EduTechHub. All rights reserved.</p>
    </footer>
</body>
</html>