function loadContent(courseTitle) {
    var xhttp = new XMLHttpRequest();
    
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("content").innerHTML = this.responseText;
        }
    };
    
    xhttp.open("GET", "get_content.php?title=" + courseTitle, true);
    xhttp.send();
}

loadContent("Course Title 1");
